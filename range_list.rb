class RangeList
  def initialize
    @range_list = []
  end

  def add(range)
    @range_list << Range.new(range.min, range.max, true)
    range_arr = convert_range_list_to_array(@range_list)
    full_arr = Array.new(range_arr.max, false)
    generate_range_list(full_arr, range_arr)
  end

  def remove(range)
    return if @range_list.size == 0
    remove_arr = Range.new(range.min, range.max).to_a[0...-1]
    range_arr = convert_range_list_to_array(@range_list)
    full_arr = Array.new(range_arr.max, false)
    range_arr = range_arr - remove_arr
    generate_range_list(full_arr, range_arr)
  end

  def print
    @range_list.map { |item| "[#{item.min}, #{item.max + 1})" }.join(" ")
  end

  private

  def generate_range_list(full_arr, range_arr)
    range_arr.each { |item| full_arr[item - 1] = true }
    @range_list = convert_array_to_range_list(full_arr)
  end

  def convert_range_list_to_array(range_list)
    range_list.map(&:to_a).flatten
  end

  # full_arr eg: [true, true, true, true, false]
  def convert_array_to_range_list(full_arr)
    return [] if full_arr.nil? || full_arr.size == 0
    range_list, start_index, end_index = [], 0, 0
    full_arr.each_with_index do |item, index|
      if item
        end_index += 1
        range_list << Range.new(start_index + 1, end_index) if index == full_arr.size - 1
      else
        range_list << Range.new(start_index + 1, end_index) if start_index != end_index
        start_index = index + 1
        end_index = index + 1
      end
    end
    range_list
  end
end
